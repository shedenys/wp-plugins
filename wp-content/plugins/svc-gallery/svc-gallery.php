<?php
/*
Plugin Name: Галерея
Description: Используйте шорткод вида: [gallery ids="1,2,3"], где в атрибуте ids указывайте ID картинок
Plugin URI: -
Author: -
Author URI: -
*/

// Удаляем функции, которые привязаны к шорткоду
remove_shortcode('gallery');
// Добавляем шорткод и хук для него
add_shortcode('gallery', 'svc_gallery');
// Подключаем необходимые стили и скрипты
add_action('wp_enqueue_scripts', 'svc_styles_scripts');

/*
 * Добавляем пользовательские настройки
 */
add_action('admin_init', 'svc_gallery_options');
function svc_gallery_options() {

 register_setting('general', 'svc_gallery_options');

 add_settings_section('gallery_section_id', 'Опции галереи', '', 'general');

 add_settings_field('svc_gallery_option_title', 'Навзание галереи', 'svc_gallery_title_cb', 'general', 'gallery_section_id');
 add_settings_field('svc_gallery_option_text', 'Текст при отсутствии картинок', 'svc_gallery_text_cb', 'general', 'gallery_section_id');
}
function svc_gallery_title_cb() {
	$options = get_option('svc_gallery_options');
	?>
	<input type="text" name="svc_gallery_options[svc_gallery_option_title]" id="svc_gallery_option_title" value="<?php echo esc_attr($options['svc_gallery_option_title']); ?>" class="regular-text">
	<?php
}
function svc_gallery_text_cb() {
	$options = get_option('svc_gallery_options');
	?>
	<input type="text" name="svc_gallery_options[svc_gallery_option_text]" id="svc_gallery_option_text" value="<?php echo esc_attr($options['svc_gallery_option_text']); ?>" class="regular-text">
	<?php
}

function svc_styles_scripts() {

	/*
	 * Регистрируем стили и скрипты
	 */
	wp_register_script('svc-lightbox-js', plugins_url('js/lightbox.min.js', __FILE__), array('jquery'));
	wp_register_style('svc-lightbox', plugins_url('css/lightbox.css', __FILE__));
	wp_register_style('svc-lightbox-style', plugins_url('css/lightbox-style.css', __FILE__));

	/*
	 * Подключаем стили и скрипты
	 */
	wp_enqueue_script('svc-lightbox-js');
	wp_enqueue_style('svc-lightbox');
	wp_enqueue_style('svc-lightbox-style');
}

function svc_gallery($atts) {
	// Получаем пользовательские настройки
	$options = get_option('svc_gallery_options');

	$img_id = explode(',', $atts['ids']);
	if(!$img_id[0]) return '<div class="svc-gallery"><h3>'.$options['svc_gallery_option_title'].'</h3>'.$options['svc_gallery_option_text'].'</div>';
	$html = '<div class="svc-gallery"><h3>'.$options['svc_gallery_option_title'].'</h3>';
	foreach ($img_id as $item) {
		$img_data = get_posts(array(
			'p' => $item,
			'post_type' => 'attachment'
		));

		// Описание картинки
		$img_desc = $img_data[0]->post_content;
		// Подпись картинки
		$img_caption = $img_data[0]->post_excerpt;
		// Тайтл картинки
		$img_title = $img_data[0]->post_title;
		// Тумбочка картинки
		$img_thumb = wp_get_attachment_image_src($item);
		// Оригинал картинки
		$img_full = wp_get_attachment_image_src($item, 'full');

		$html .= "<a href='{$img_full[0]}' data-lightbox='gallery' data-title='{$img_caption}'><img src='{$img_thumb[0]}' width='{$img_thumb[1]}' height='{$img_thumb[2]}' alt='{$img_title}'></a>";
	}
	$html .= '</div>';
	return $html;
}