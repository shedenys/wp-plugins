<?php
/*
Plugin Name: Похожие записи
Description: Плагин выводит ссылки на несколько записей той же рубрики
Plugin URI: -
Author: -
Author URI: -
*/

add_filter('the_content', 'svc_related_posts');
// Подключаем стили и скрипты
add_action('wp_enqueue_scripts', 'wp_register_styles_scripts');
function wp_register_styles_scripts() {
	// Регистрируем скрипты и стили
	wp_register_script('svc-jquery-tools-js', plugins_url('js/jquery.tools.min.js', __FILE__), ['jquery']);
	wp_register_script('svc-scripts-js', plugins_url('js/svc-scripts.js', __FILE__), ['jquery']);
	wp_register_style('svc-related-posts', plugins_url('css/svc_style.css', __FILE__));
	// Подключаем стили и скрипты
	wp_enqueue_script('svc-jquery-tools-js');
	wp_enqueue_script('svc-scripts-js');
	wp_enqueue_style('svc-related-posts');
}


function svc_related_posts($content) {
	// Проверка, находимся ли мы на странице отдельной записи
	if(!is_single()) return $content;

	// Получаем ID записи
	$id = get_the_ID();
	// Получаем связанные категории записи
	$categories = get_the_category($id);
	// Сохраняем ID
	foreach($categories as $category) {
		$cats_id[] = $category->cat_ID;
	}

	$related_posts = new WP_Query([
		'posts_per_page' => 5,
		'category__in' => $cats_id,
		'orderby' => 'rand',
		'post__not_in' => [$id]
	]);

	if ($related_posts->have_posts()) {
		$content .= '
			<div class="related-posts">
				<h3>Возможно вас заинтересуют эти записи:</h3>';
				while($related_posts->have_posts()) {
					$related_posts->the_post();
					// Есть ли у записи миниатюра
					if(has_post_thumbnail()) {
						$img = get_the_post_thumbnail(get_the_ID(), [100, 100], ['alt' => get_the_title(), 'title' => get_the_title()]);
					}
					else {
						$img = '<img src="'.plugins_url('images/no_img.jpg', __FILE__).'" alt="'.get_the_title().'" title="'.get_the_title().'" width="100" height="100">';

					}
					$content .= '<a href="'.get_permalink().'">'.$img.'</a>';
				}
				$content .= '
			</div>
		';
		// Сброс пользовательской выборки
		wp_reset_query();
	}

	return $content;
}